import Axios from 'axios';

export default class ApiRequestManager {
    static sharedManager = null;

    static getInstance() {
        if (ApiRequestManager.sharedManager == null) {
            ApiRequestManager.sharedManager = new ApiRequestManager();
        }
        return ApiRequestManager.sharedManager;
    }

    requestServer(requestType, requestBody, urlString, header) {
        return new Promise((resolve, reject) => {
            fetch(urlString, {
                method: requestType,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    "authorization": header,
                },
                body: requestBody
            }).then((response) => {
                return response.json();
            }).then((responseData) => {
                if (responseData.type === "success") {
                    resolve(responseData)
                } else {
                    reject(responseData.message)
                }
            }).catch(err => {
                if (err == "TypeError: Network request failed") {
                    reject("Bad Network, Please try again")
                } else {
                    reject(err)
                }
            });
        });
    }

    requestServerAxios(requestBody, urlString, header) {
        return new Promise((resolve, reject) => {
            Axios.post(urlString, requestBody, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    "authorization": header,
                },
                body: requestBody
            }).then((response) => {
                return response;
            }).then((responseData) => {
                if (responseData.type === "success") {
                    resolve(responseData)
                } else {
                    reject(responseData.message)
                }
            }).catch(err => {
                if (err == "TypeError: Network request failed") {
                    reject("Bad Network, Please try again")
                } else {
                    reject(err)
                }
            });
        });
    }

    requestServerReg(requestType, requestBody, urlString, header) {
        return new Promise((resolve, reject) => {
            fetch(urlString, {
                method: requestType,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    authorization: header,
                },
                body: requestBody
            }).then((response) => {
                return response.json();
            }).then((responseData) => {
                if (responseData.status === "success") {
                    resolve(responseData)
                } else {
                    reject(responseData)
                }
            }).catch(err => {
                reject(err)
            });
        });
    }

    requestServerandroid(requestType, requestBody, urlString, header) {
        return new Promise((resolve, reject) => {
            fetch(urlString, {
                method: requestType,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    authorization: header,
                },
                body: requestBody
            }).then((response) => {
                if (response.ok == false) {
                    reject(Error('Respponse not in the correct format'));
                }
                return response.json();
            }).then((responseData) => {
                if (responseData.status === "success") {
                    resolve(responseData)
                } else {
                    reject(responseData.message)
                }
            }).catch(err => {
                reject(err)
            });
        });
    }

    requestServerProfile(requestType, urlString, header, requestBody) {
        return new Promise((resolve, reject) => {
            fetch(urlString, {
                method: requestType,
                headers: {
                    'Content-Type': 'multipart/form-data',
                    "authorization": header,
                },
                body: requestBody
            }).then((response) => {
                return response.json();
            }).then((responseData) => {
                if (responseData.type === "success") {
                    resolve(responseData)
                } else {
                    reject(responseData.message)
                }
            }).catch(err => {
                reject(err)
            });
        });
    }

    requestServerGet(urlString, authToken) {
        return new Promise((resolve, reject) => {
            fetch(urlString, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'authorization': authToken,
                },
            }).then((response) => {
                if (response.ok == false) {
                    reject(Error('Response not in the correct format'));
                }
                return response.json();
            }).then((responseData) => {
                resolve(responseData.message);
            }).catch(err => {
                reject(err)
            });
        });
    }

    apihitbyAxios(requestType, requestBody, urlString, header) {
        return new Promise((resolve, reject) => {
            Axios({
                method: requestType, url: urlString,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    'Authorization': header,
                },
                data: requestBody
            }).then((response) => {
                if (response.ok == false) {
                    reject(Error('Response not in the correct format'));
                }
                return response;
            }).then((responseData) => {
                resolve(responseData.message);
            }).catch(err => {
                reject(err)
            });
        });
    }

    requestServerGetAxios(requestBody, urlString, header) {
        return new Promise((resolve, reject) => {
            Axios.get(urlString, requestBody, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                    "authorization": header,
                },
                body: requestBody
            }).then((response) => {
                return response;
            }).then((responseData) => {
                if (responseData.type === "success") {
                    resolve(responseData)
                } else {
                    reject(responseData.message)
                }
            }).catch(err => {
                if (err == "TypeError: Network request failed") {
                    reject("Bad Network, Please try again")
                } else {
                    reject(err)
                }
            });
        });
    }
}

class CustomError extends Error { };
const a = new CustomError();
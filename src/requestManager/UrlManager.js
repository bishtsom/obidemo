const SUPERURL = {
  //devlopment
  //APISUPERBASEURL: 'http://192.168.1.115',

  //Live
  APISUPERBASEURL: "http://3.17.203.6"
};

const BASE_SOCKET_URL = {
  SOCKETBASEURL: SUPERURL.APISUPERBASEURL + ":3000/"
};

const APIBASE = {
  APIBASEURL: BASE_SOCKET_URL.SOCKETBASEURL + "api/"
};

export const APIURLS = {
  LOGIN_URL: APIBASE.APIBASEURL + "user/login",
  REGISRATION_URL: APIBASE.APIBASEURL + "user/register",
  FORGOTPASSWORD_URL: APIBASE.APIBASEURL + "user/forgotpassword",
  VARIFYOTP_URL: APIBASE.APIBASEURL + "user/verifyotp",
  RESENDOTP_URL: APIBASE.APIBASEURL + "user/resendotp",
  FACEBOOKLOGIN_URL: APIBASE.APIBASEURL + "user/facebook",
  PROFILEUPDATE_URL: APIBASE.APIBASEURL + "user/profile",
  UPDATEPHONE_URL: APIBASE.APIBASEURL + "user/updatephone",
  GETPROFILE_URL: APIBASE.APIBASEURL + "user/profile",
  GENERATEBRAINTREETOKEN_URL: APIBASE.APIBASEURL + "user/payment/token",
  ADDCARD_URL: APIBASE.APIBASEURL + "user/addcard",
  INITRIDE_URL: APIBASE.APIBASEURL + "user/initbooking",
  CONFIRMRIDE_URL: APIBASE.APIBASEURL + "user/startbooking",
  CANCEL_RIDE_URL: APIBASE.APIBASEURL + 'user/cancelbooking',
  GET_RIDER_ACTIVE_RIDE_URL: APIBASE.APIBASEURL + 'user/getridedata',
  GET_RIDER_ACTIVE_RIDE_DATA_URL: APIBASE.APIBASEURL + 'user/getridedata',
  EDIT_RIDE_URL: APIBASE.APIBASEURL + 'user/editride',

  //DRIVER
  DRIVER_LOGIN_URL: APIBASE.APIBASEURL + "driver/login",
  DRIVER_VARIFYOTP_URL: APIBASE.APIBASEURL + "driver/verifyotp",
  DRIVER_RESENDOTP_URL: APIBASE.APIBASEURL + "driver/resendotp",
  DRIVER_PROFILE_URL: APIBASE.APIBASEURL + "driver/profile",
  DRIVER_DRIVERSTATUS_URL: APIBASE.APIBASEURL + "driver/driverstatus",
  DRIVER_ACCEPT_RIDE_URL: APIBASE.APIBASEURL + "driver/acceptride",
  DRIVER_CANCEL_RIDE_URL: APIBASE.APIBASEURL + "driver/cancelride",
  ARRIVE_TO_URL: APIBASE.APIBASEURL + 'driver/arrive',
  PICK_UP_URL: APIBASE.APIBASEURL + 'driver/pickup',
  ARRIVED_FIRST_STOP_URL: APIBASE.APIBASEURL + 'driver/arrivedStop',
  START_FOR_SECOND_STOP_URL: APIBASE.APIBASEURL + 'driver/readytogo',
  DROP_OFF_URL: APIBASE.APIBASEURL + 'driver/drop',
  GET_DRIVER_ACTIVE_RIDE_URL: APIBASE.APIBASEURL + 'driver/getridedata',
  GET_DRIVER_ACTIVE_RIDE_DATA_URL: APIBASE.APIBASEURL + 'driver/getridedata',
};

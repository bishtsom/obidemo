import { AccessToken } from 'react-native-fbsdk';
import { LoginManager } from "react-native-fbsdk";

export const faceLogin = () => {
  var promise = new Promise(function (resolve, reject) {

    /**
     * This method is used to reterieve the device token 
     */
    LoginManager.logInWithReadPermissions(["public_profile"]).then(
      function (result) {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then(
            (data) => {
              console.log("token: ", data.accessToken.toString())
              var token = data.accessToken.toString();
              resolve(token)
            }
          )
        }
      }, function (error) {
        console.log("Login fail with error: " + error);
      });

    _responseInfoCallback = (error, result) => {
      if (error) {
        reject(error)
      } else {
        console.log("Login Email " + result.name);
        resolve(result)
      }
    }
  })
  return promise;
}




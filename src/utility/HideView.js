
import React from 'react';
import { View, StyleSheet } from 'react-native';

const HideView = (props) => {
    const { children, hide } = props;
    if (hide) {
        return null;
    }
    return (
        <View {...this.props} style={styles.picture}>
            {children}
        </View>
    );
};

const styles = StyleSheet.create({
    picture: {
        width: null,
        height: null,
    }
});

export default HideView;
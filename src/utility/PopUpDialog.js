import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Dialog, { DialogContent } from 'react-native-popup-dialog';

export default class PopUpDialog extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <Dialog visible={this.props.visible} onTouchOutside={() => { this.setState({ visible: false }) }} backgroundColor='black'>
                <DialogContent style={{ height: this.state.flatlistSize }}>
                    <View style={{ flexDirection: "column" }}>
                        <Text>Please enter your email id</Text>
                        <View style={{ flexDirection: 'row' }}>
                            <TouchableOpacity onPress={this.props.cancel.bind(this)}>
                                <Text>Cancel</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.props.okay.bind(this)}>
                                <Text>Ok</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </DialogContent>
            </Dialog>
        );
    }
}
const string = {

    // phone verification page
    oneTimePassText: 'One Time Password (OTP) has been sent to your mobile, please enter here to register your phone number.',
    submit: 'Submit',
    resendOtp: 'Resend OTP',
    notReceived: 'Not received? ',

    // signUp page 
    enterEmail: 'Please enter your Email id',
    enterValidEmail: 'Please enter your valid Email id',
    enterFirstName: 'Please enter your First Name',
    enterLastName: 'Please enter your Last Name',
    enterPassword: 'Please enter your Password',
    passwordLength: 'Pasword should be of atleast 8 characters',
    confirmPassword: 'Please confirm your password',
    passwordMismatch: 'Password and Confirm password does not match',
    enterPhoneNumber: 'Please enter your phone number',
    phoneNumberLength: 'Enter a valid phone number',

    // refer and earn
    referAndEarn: "Refer & Earn",
    referHeading: "Refer to your friends and enjoy more.",
    referText: "You may invite your family and friends to become new Users directly through the Obi Platform by entering or providing Obi with your invitee's contact information"
}
export default string;
import React, { Component } from 'react';
import { Image, Platform, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import colors from './colors';
import { scaledHeight, scaledWidthPercent } from './deviceDimensions';
import fonts from './fonts';
const android = Platform.OS == "android";

export default class CustomHeader extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View style={{ flexDirection: 'row', backgroundColor: colors.themeColor, height: scaledHeight(50), width: scaledWidthPercent(100) }}>
                <View style={{ flex: 4, flexDirection: 'row', alignItems: 'center', justifyContent: "center", marginLeft: 20, marginRight: 20 }}>
                    <TouchableOpacity onPress={this.props.leftClick} style={{ flex: 1, height: 20, alignItems: 'center', justifyContent: "center" }}>
                        <Image source={this.props.LeftImage} style={{ height: 15, width: 15, marginRight: 5 }} />
                    </TouchableOpacity>
                    <Text style={{ flex: 1.8, color: 'white', fontSize: 15, fontFamily: fonts.regularFonts }}>{this.props.LeftHeading}</Text>

                    <TouchableOpacity onPress={this.props.rightClick} style={{ flex: 1.2, flexDirection: 'row', height: 40, alignItems: 'center', justifyContent: "center" }}>
                        <Text style={{ color: 'white', fontSize: 13, marginRight: 10, fontFamily: fonts.regularFonts }}>{this.props.RightHeading}</Text>
                        <Image source={this.props.RightImage} style={{ height: 15, width: 15 }} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
const fonts = {
    lightFont: "EncodeSans-Light",
    boldFont: "EncodeSans-Bold",
    mediumFont: "EncodeSans-Medium",
    regularFont: "EncodeSans-Regular",
}
export default fonts;
import { Dimensions } from "react-native";

export var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
export const scaledWidthPercent = size => { return ((deviceWidth * size) / 100) }
export const scaledHeightPercent = size => { return ((deviceHeight * size) / 100) }

export const scaledWidth = size => { return (deviceWidth * ((size * .266) / 100)) }
export const scaledHeight = size => { return (deviceHeight * ((size * .149) / 100)) }

export default {
  //LOGIN DIMENSIONS//
};

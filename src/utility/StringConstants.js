export default {
    key_user_token: "user_token",
    LATITUDE_DELTA: 0.0922,
    LONGITUDE_DELTA: 0.0421,
    key_home_region: "home_region",
    map_api_key: "AIzaSyDRj-SmztFjxXpuzNc00mAPatuX-Q_2z_A",
    //SOCKET_BASE_URL: "http://192.168.1.115:3000",   // for testing server
    SOCKET_BASE_URL: "http://3.17.203.6:3000",     // for staging/live server
    Rider: 'rider',
    Driver: 'driver',
    DRIVERLOCATION: 'updateLocationDriver',
    RIDEREQUEST: 'rideRequest',
    RIDEACCEPTED: 'rideAccepted',
    UPDATEDRIVERLOC: 'updateLocationDriver',
    GETDRIVERLOC: 'getDriverLocation',
    CABNOTFOUND: 'rideNotFound',
    RIDECANCELEDBYRIDER: 'rideCancelled',
    RIDEEDITED: 'rideEdited',
    RIDECOMPLETED: 'rideCompleted',
    RIDERARRIVED: 'rideArrived',
    ARRIVEDATSTOPONE: 'rideArrivedAtStop',
    READYTOGO: 'rideReadyToGo',
    RIDESTARTED: 'rideStarted',
};
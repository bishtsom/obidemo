import { AsyncStorage } from "react-native";
tokenValue = null;

export default class AppStorage {
  static myInstance = null;
  drawer_data = {};
  fcm_token = null;
  networkStatus = false;

  static getInstance() {
    if (this.myInstance == null) {
      this.myInstance = new AppStorage();
    }
    return this.myInstance;
  }

  /**
   * stores driver data in local storage.
   * @param {stores the data with this key} key_to_be_paired
   * @param {data to be stored} data_to_save
   */
  async setStoreData(key_to_be_paired, data_to_save) {
    try {
      console.log("storing key: ", key_to_be_paired);
      await AsyncStorage.setItem(key_to_be_paired, data_to_save);
    } catch (error) {
      console.log("error while storing: ", error);
    }
  }

  /**
   * returns data stored with the key
   * @param {returns the data stored with this key} key_to_be_fetched
   */
  async getStoreData(key_to_be_fetched) {
    try {
      const value = await AsyncStorage.getItem(key_to_be_fetched);
      if (value !== null) {
        // We have data!!
        console.log("value is " + value);
        return value;
      }
    } catch (error) {
      // Error retrieving data
      console.log("error in fetching async: " + error);
      return null;
    }
  }

  /**
   * removes the data that is stored with this key
   * @param {remove the data stored with this key} key_to_be_removed
   */
  async removeStoreData(key_to_be_removed) {
    try {
      await AsyncStorage.removeItem(key_to_be_removed);
    } catch (error) {
      // Error saving data
      console.log("error in removing: " + error);
    }
  }
}

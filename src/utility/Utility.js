import { Alert, AsyncStorage, NetInfo } from "react-native";
import Geocoder from 'react-native-geocoder';
import StringConstants from "./StringConstants";

// minimum distance for updating trip stops.
currentImage = '';
currentRideId = '';
currentRouteArray = [];

const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

export default class Utility {
  static myInstance = null;

  /**
   * @returns {Utility}
   */
  static getInstance() {
    if (this.myInstance == null) {
      this.myInstance = new Utility();
    }
    return this.myInstance;
  }

  isEmpty(obj) {
    for (var key in obj) {
      if (obj.hasOwnProperty(key)) return false;
    }
    return true;
  }

  getFormatedTime(time_in_sec) {
    if (time_in_sec > 3600) {
      var hrs = Math.floor(time_in_sec / 3600);
      var min = Math.floor((time_in_sec - hrs * 3600) / 60);
      return hrs + " hr " + min + " min";
    } else if (time_in_sec > 60) {
      var in_min = time_in_sec / 60;
      return in_min + " min";
    } else {
      return time_in_sec + " sec";
    }
  }

  getDays(time_in_sec) {
    if (time_in_sec > 86400) return Math.floor(time_in_sec / 86400);
    else return "0";
  }

  getHrs(time_in_sec) {
    if (time_in_sec > 86400) {
      var days = Math.floor(time_in_sec / 86400);
      return Math.floor((time_in_sec - days * 86400) / 3600);
    } else if (time_in_sec > 3600) return Math.floor(time_in_sec / 3600);
    else return "0";
  }

  getMints(time_in_sec) {
    if (time_in_sec > 60) {
      var hrs = Math.floor(time_in_sec / 3600);
      return Math.floor((time_in_sec - hrs * 3600) / 60);
    } else return 0;
  }

  getDistanceInMile(distance) {
    var distanceInMeter = distance;
    var distanceinMile = Math.floor(distanceInMeter * 0.000621371);
    return distanceinMile + " mi";
  }

  /**
   * initalize network connections.
   */
  startWatchingNetwork() {
    NetInfo.getConnectionInfo().then(connectionInfo => { });
    function handleFirstConnectivityChange(connectionInfo) {
      if (connectionInfo.type == "wifi") {
        Utility.getInstance().isInternetConneted = true;
      } else if (connectionInfo.effectiveType == "4g") {
        Utility.getInstance().isInternetConneted = true;
      } else if (connectionInfo.effectiveType == "3g") {
        Utility.getInstance().isInternetConneted = true;
      } else if (connectionInfo.effectiveType == "2g") {
        Utility.getInstance().isInternetConneted = true;
      } else {
        Utility.getInstance().isInternetConneted = false;
      }
    }
    NetInfo.addEventListener("connectionChange", handleFirstConnectivityChange);
  }

  disableNetworkConnection() {
    const dispatchConnected = isConnected => this.setIsConnected(isConnected);
    NetInfo.removeEventListener("change", dispatchConnected);
  }

  /**
   * sets internet connection value in global variable
   * @param {connetion value} isConnected
   */
  setIsConnected(isConnected) {
    Utility.getInstance().isInternetConneted = isConnected;
  }

  /**
   * shows no network dialong
   */
  showNoNetworkDialog() {
    if (!Utility.getInstance().isAlertShown) {
      Utility.getInstance().isAlertShown = true;

      Alert.alert(
        "No Internet Connection",
        "Internet connection is required in order to updated trip status",
        [
          {
            text: "Cancel",
            onPress: this.cancelPress.bind(this),
            style: "cancel"
          },
          { text: "Confirm", onPress: this.openSettingsPage.bind(this) }
        ],
        { cancelable: true }
      );
    }
  }

  cancelPress() {
    Utility.getInstance().isAlertShown = false;
  }

  /**
   * stores driver data in local storage.
   * @param {stores the data with this key} key_to_be_paired
   * @param {data to be stored} driver_data
   */
  async setStoreData(key_to_be_paired, driver_data) {
    try {
      console.log("storing data  " + driver_data);
      await AsyncStorage.setItem(key_to_be_paired, JSON.stringify(driver_data));
    } catch (error) {
      // Error saving data
      console.log("setStoreData error " + error);
    }
  }

  /**
   * returns data stored with the key
   * @param {returns the data stored with this key} key_to_be_fetched
   */
  async getStoreData(key_to_be_fetched) {
    try {
      const value = await AsyncStorage.getItem(key_to_be_fetched);
      if (value != null) {
        // We have data!!
        console.log("found data  " + key_to_be_fetched);
        return value;
      } else {
        return null;
      }
    } catch (error) {
      // Error retrieving data
      return "";
    }
  }

  /**
   * removes the data that is stored with this key
   * @param {remove the data stored with this key} key_to_be_removed
   */
  async removeStoreData(key_to_be_removed) {
    try {
      console.log("removing data  " + key_to_be_removed);
      await AsyncStorage.removeItem(key_to_be_removed);
    } catch (error) {
      // Error saving data
    }
  }

  /**
   * checks weather an email is vallid or not.
   * @param {email address that need to be verified} email_address
   */
  isEmailValid(email_address) {
    // let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    // return reg.test(email_address);
    if (email_address.length > 3) return true;
    return false;
  }

  /**
   * Convert seconds into hours, minutes and seconds
   * @param {seconds} d
   */
  secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor((d % 3600) / 60);
    var s = Math.floor((d % 3600) % 60);

    return (
      (h < 10 ? "0" + h : h) +
      ":" +
      (m < 10 ? "0" + m : m) +
      ":" +
      (s < 10 ? "0" + s : s)
    );
  }

  isEmptyField(item_to_check) {
    if (
      item_to_check == null ||
      item_to_check == undefined ||
      item_to_check == "" ||
      item_to_check == "null"
    )
      return true;
    return false;
  }

  getCurrentMonth() {
    return new Promise((resolve, reject) => {
      try {
        resolve(monthNames[new Date().getMonth()]);
      } catch (error) {
        reject(error)
      }
    });
  }

  getDetailedAddress(NY) {
    return new Promise((resolve, reject) => {
      Geocoder.geocodePosition(NY).then(res => {
        // res is an Array of geocoding object (see below)
        console.log("address", res[0]);
        resolve(res[0]);
      })
        .catch(err => reject(err))
    });
  }

  getLatLongViaAddress(address) {
    console.log("SEnded ADD", address);
    return new Promise((resolve, reject) => {
      fetch("https://maps.googleapis.com/maps/api/geocode/json?address=" + address + "&key=AIzaSyD5FtmP5A0VbdOc025WRXHtPjNHdDWVGGc").then(response => {

        restData = response.json();
        restData.then(data => {
          console.log("THE RES", data.results[0].geometry.location.lat + "," + data.results[0].geometry.location.lng);
          resolve(data.results[0].geometry.location)
        });
      }).catch(err => reject(err))
    });
  }

  getCurrentLocation() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.watchPosition((position) => {

        let initalVal = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: StringConstants.LATITUDE_DELTA,
          longitudeDelta: StringConstants.LONGITUDE_DELTA,
        }
        resolve(initalVal);

      }, (error) => reject(error))
    })
  }

  getOneTimeCurrentLocation() {
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(
        position => {
          var NY = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
          };

          resolve(NY)
          // this.setState({ location });
        },
        error => { reject(error) },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
      );
    })
  }
}
module.export = Utility;


const colors = {
    gray: "#696969",
    lightBlack: "#323d32",
    buttonBorderColor: '#32A582',
    themeColor:'#32A582',
    facebookColor: '#375B94',
}
export default colors;
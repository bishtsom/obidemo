import SocketIOClient from "socket.io-client";
import StringConstants from "./StringConstants";
const moment = require("moment");
moment.suppressDeprecationWarnings = true;
var isSocket_manual_close = false;

export default class SocketConnectionHandler {
  static myInstance = null;

  /**
   * @returns {Sockethandler instace}
   */
  static getInstance() {
    if (this.myInstance == null) {
      this.myInstance = new SocketConnectionHandler();
    }
    return this.myInstance;
  }

  /**
   * connects with the socket server
   */
  connectToSocketServer(accessToken, type, driverLocation) {
    if (isSocket_manual_close) return;
    close_threshold = -1;

    if (this.socket != null && this.socket != undefined) {
      if (this.socket.connected) return;
    }

    if (this.socket == undefined) {
      this.socket = SocketIOClient(
        StringConstants.SOCKET_BASE_URL, {
          query: {
            authToken: accessToken,
            type: type
          }
        }
      );
      this.socket.connect();

      this.socket.on("connect", () => {
        var _this = this;
        setTimeout(function () {
          _this.sendEmitMessge(driverLocation);
        }, 1000)
      });

      this.socket.on("error", error => {
        console.log("Error in socket connection" + error);
      });

    } else if (!this.socket.connected) {
      this.socket.connect();
      this.socket.on("connect", () => {
        console.log("connected with server");
      });

      this.socket.on("disconnect", () => {
        console.log("disconnected with server");
      });
    }
  }

  /**
   * sends message to the server passed to it
   */
  sendEmitMessge(json_message) {
    if (this.socket != null && this.socket != undefined && this.socket.connected) {
      this.socket.emit(StringConstants.DRIVERLOCATION, json_message);
    }
  }

  /**
  * sends message to the server passed to it
  */
  emitMessge(key, json_message) {
    if (this.socket != null && this.socket != undefined && this.socket.connected) {
      console.log("GGG", key);
      this.socket.emit(key, json_message);
    }
  }

  /**
   * is invoked if any message is received
   */
  onMessageReceived(type) {
    if (this.socket != null && this.socket != undefined && this.socket.connected) {
      return new Promise((resolve, reject) => {
        this.socket.on(type, data => {
          resolve(data)
        });
      }).catch(err => reject(err))
    }
    return null;
  }

  /**
   * is invoked if any message is received
   */
  closeSocketConnection() {
    if (this.socket != null && this.socket != undefined) {
      this.socket.disconnect();
      this.socket = null;
    }
  }
}
module.export = SocketConnectionHandler;

import React, { Component } from 'react';
import { Image, Text, View } from 'react-native';
import colors from './colors';
import { scaledHeight, scaledWidthPercent } from './deviceDimensions';
import fonts from './fonts';

export default class DriverDestTopView extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View style={{ flexDirection: 'row', backgroundColor: "white", height: scaledHeight(70), width: scaledWidthPercent(100) }}>
                <View style={{ flex: 3, flexDirection: 'row', alignItems: 'center', justifyContent: "center", marginLeft: 20, marginRight: 5 }}>
                    <View style={{ flex: 2.5, justifyContent: "center" }}>
                        <Image source={this.props.LeftImage} />
                        <Text style={{ marginTop: 5, fontSize: 13, fontFamily: fonts.regularFonts }}>{this.props.LeftText}</Text>
                    </View>

                    <View style={{ width: 1, height: scaledHeight(50), backgroundColor: colors.gray, marginRight: 5 }} />

                    <View onPress={this.props.rightClick} style={{ flex: .5, alignItems: 'center', justifyContent: "center" }}>
                        <Image source={this.props.RightImage} />
                        <Text style={{ marginTop: 5, fontSize: 18, fontFamily: fonts.boldFont }}>{this.props.RightText}</Text>
                    </View>
                </View>
            </View>
        );
    }
}
import React, { Component } from 'react';
import { Image, Platform, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { scaledHeight, scaledWidthPercent } from './deviceDimensions';
import fonts from './fonts';

export default class CustomHeaderDriver extends Component {
    constructor(props) {
        super(props)
        this.state = {

        }
    }

    render() {
        return (
            <View style={{ flexDirection: 'row', backgroundColor: "white", height: scaledHeight(50), width: scaledWidthPercent(100) }}>
                <View style={{ flex: 4, flexDirection: 'row', alignItems: 'center', justifyContent: "center", marginLeft: 20, marginRight: 20 }}>
                    <TouchableOpacity onPress={this.props.leftClick} style={{ flex: 1, height: 20, width: 50, alignItems: 'center', justifyContent: "center" }}>
                        <Image style={{ height: 20, width: 50 }} source={this.props.LeftImage} />
                    </TouchableOpacity>
                    <View style={{ alignItems: 'center', flex: 1.8, color: 'white', }}>
                        <TouchableOpacity onPress={this.props.centerClick}>
                            <Image source={this.props.CenterImage} />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={this.props.rightClick} style={{ flex: 1.2, flexDirection: 'row', height: 40, alignItems: 'center', justifyContent: "center" }}>
                        <Text style={{ color: 'white', fontSize: 13, marginRight: 10, fontFamily: fonts.regularFonts }}>{this.props.RightHeading}</Text>
                        <Image source={this.props.RightImage} style={{ height: 15, width: 20 }} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
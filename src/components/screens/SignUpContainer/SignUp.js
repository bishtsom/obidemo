import React, { Component } from 'react';
import { Image, ImageBackground, Text, TextInput, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PhoneInput from 'react-native-phone-input';
import { SafeAreaView } from 'react-navigation';
import ApiRequestManager from '../../../requestManager/ApiRequestManager';
import { APIURLS } from '../../../requestManager/UrlManager';
import colors from '../../../utility/colors';
import fonts from '../../../utility/fonts';
import imagesPath from '../../../utility/imagesPath';
import Loader from '../../../utility/Loader';
import string from '../../../utility/string';
emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

export default class SignUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: null,
            userName: null,
            lastName: null,
            phoneNumber: null,
            password: null,
            confirmPassword: null,
            loaderVisible: false,
            referCode: "",
            specialChars: "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=",
        }
    }

    /**
     * Checking the empty and incorrect labels
     */
    signUpPageEntriesValidation() {
        if (this.state.email === null || this.state.email.trim === "") {
            alert(string.enterEmail)
        } else if (emailReg.test(this.state.email) === false) {
            alert(string.enterValidEmail)
        } else if (this.state.userName === null || this.state.userName.trim === "") {
            alert(string.enterFirstName)
        } else if (this.checkForSpecialChar(this.state.userName)) {
            alert('Enter valid first name')
        } else if (this.state.lastName === null || this.state.lastName.trim === "") {
            alert(string.enterLastName)
        } else if (this.checkForSpecialChar(this.state.lastName)) {
            alert('Enter valid last name')
        } else if (this.state.phoneNumber == null || this.state.phoneNumber === "") {
            alert(string.enterPhoneNumber)
        } else if (this.state.phoneNumber.length < 6) {
            alert(string.phoneNumberLength)
        } else {
            this.signUp()
        }
    }

    /**
     * login to the app as a rider
     */
    login() {
        this.clearSignUpFields();
        this.props.navigation.navigate("login");
    }

    /**
     * signup to the app with user entries
     */
    signUp() {
        this.setState({ loaderVisible: true });
        let body = {
            email: this.state.email,
            userName: this.state.userName,
            lastName: this.state.lastName,
            phone: this.phonee.getValue() + this.state.phoneNumber,
        }
        console.log('body is ' + body);
        ApiRequestManager.getInstance().requestServer("POST", "email=" + this.state.email + "&userName=" + this.state.userName + "&lastName=" + this.state.lastName + "&phone=" + this.phonee.getValue().replace("+", "") + this.state.phoneNumber + "&referCode=" + this.state.referCode + "", APIURLS.REGISRATION_URL, "").then((response) => {
            console.log("Result is ", JSON.stringify(response));
            this.setState({ loaderVisible: false });
            var _this = this;
            setTimeout(function () {
                _this.clearSignUpFields();
                _this.props.navigation.navigate("verification", { "userId": response.userId, "type": 'User' });
            }, 1000)
        }).catch((error) => {
            this.setState({ loaderVisible: false });
            setTimeout(function () {
                alert(error);
            }, 1000)
        })
    }

    /**
     * clear signup fields
     */
    clearSignUpFields() {
        this.setState({ email: "", userName: "", lastName: "", phoneNumber: "", password: "", confirmPassword: "" });
    }

    onPressFlag() {
        alert("date select")
        this.countryPicker.openModal()
    }

    /**
     * check if special characters exist
     * @param {} name 
     */
    checkForSpecialChar(name) {
        for (i = 0; i < this.state.specialChars.length; i++) {
            if (name.indexOf(this.state.specialChars[i]) > -1) {
                return true
            }
        }
        return false;
    }

    /**
     * UI part
     */
    render() {
        return (
            <SafeAreaView style={{ flex: 1, flexDirection: 'column' }}>
                <ImageBackground source={imagesPath.backGroundImage} style={{ flex: 1, flexDirection: 'column' }}>
                    <KeyboardAwareScrollView>
                        <Image source={imagesPath.logoImage} style={{ alignSelf: 'center', marginTop: 40 }} />

                        <View style={{ flex: 1, justifyContent: 'center', flexDirection: 'column', alignItems: "center", alignSelf: 'center', alignContent: 'center', marginLeft: 40, marginRight: 40, marginBottom: 20 }}>

                            <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 30, borderColor: '#32A582', borderWidth: 1 }}>
                                <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                                    <Image style={{ height: 14, width: 18 }} source={imagesPath.email} color="#000" />
                                </View>
                                <TextInput keyboardType='email-address' style={{ flex: 0.9, height: 42, fontFamily: fonts.regularFont }} placeholder="Email id" placeholderTextColor={colors.gray} onChangeText={email => this.setState({ email })} value={this.state.email} />
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                                    <Image style={{ height: 18, width: 12 }} source={imagesPath.userName} color="#000" />
                                </View>
                                <TextInput maxLength={15} style={{ flex: 0.9, height: 42, fontFamily: fonts.regularFont }} placeholder="First Name" placeholderTextColor={colors.gray} onChangeText={userName => this.setState({ userName })} value={this.state.userName} />
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                                    <Image style={{ height: 18, width: 12 }} source={imagesPath.userName} color="#000" />
                                </View>
                                <TextInput maxLength={15} style={{ flex: 0.9, height: 42, fontFamily: fonts.regularFont }} placeholder="Last Name" placeholderTextColor={colors.gray} onChangeText={lastName => this.setState({ lastName })} value={this.state.lastName} />
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                                    <Image style={{ height: 16, width: 16 }} source={imagesPath.telephone} color="#000" />
                                </View>
                                <PhoneInput style={{ margin: 2, flex: 0.1 }} ref={ref => {
                                    this.phonee = ref;
                                }} />
                                <TextInput style={{ flex: 0.8, height: 42, fontFamily: fonts.regularFont }} placeholder="Phone Number" placeholderTextColor={colors.gray} onChangeText={phoneNumber => this.setState({ phoneNumber })} value={this.state.phoneNumber} maxLength={10} keyboardType='numeric' />
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                                    <Image style={{ height: 18, width: 12 }} source={imagesPath.userName} color="#000" />
                                </View>
                                <TextInput maxLength={6} style={{ flex: 0.9, height: 42, fontFamily: fonts.regularFont }} placeholder="Promo Code" placeholderTextColor={colors.gray} onChangeText={referCode => this.setState({ referCode })} value={this.state.referCode} />
                            </View>

                            <View style={{ flexDirection: 'column', alignItems: 'center', alignSelf: 'center', marginTop: 30, marginBottom: 30 }}>
                                <Text style={{ color: 'black', fontSize: 12, fontFamily: fonts.regularFont }}>By clicking Sign Up, you agree to the</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity>
                                        <Text style={{ color: 'black', fontSize: 12, textDecorationLine: 'underline', fontFamily: fonts.regularFont }}>Terms and conditions</Text>
                                    </TouchableOpacity>
                                    <Text style={{ color: 'black', fontSize: 12, fontFamily: fonts.regularFont }}> of OBI</Text>
                                </View>
                            </View>

                            <Loader loading={this.state.loaderVisible} />

                            <TouchableOpacity onPress={this.signUpPageEntriesValidation.bind(this)} style={{ alignItems: "center", justifyContent: "center", height: 50, width: 250, borderRadius: 25, backgroundColor: '#32A582' }}>
                                <Text style={{ fontSize: 20, color: 'white', fontFamily: fonts.boldFont, color: 'white' }}>Sign up</Text>
                            </TouchableOpacity>

                            <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center', marginTop: 50, marginBottom: 30 }}>
                                <Text style={{ color: 'black', fontSize: 12, fontFamily: fonts.regularFont }}>Already Member? </Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <TouchableOpacity onPress={this.login.bind(this)}>
                                        <Text style={{ color: 'black', fontSize: 12, textDecorationLine: 'underline', fontFamily: fonts.boldFont }}>Login here</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </ImageBackground>
            </SafeAreaView>
        );
    }
}

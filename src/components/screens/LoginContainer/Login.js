import React, { Component } from 'react';
import { AsyncStorage, Image, ImageBackground, Text, TextInput, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PhoneInput from 'react-native-phone-input';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import { NavigationActions, SafeAreaView, StackActions } from 'react-navigation';
import ApiRequestManager from '../../../requestManager/ApiRequestManager';
import { APIURLS } from '../../../requestManager/UrlManager';
import colors from '../../../utility/colors';
import { scaledHeight, scaledWidthPercent } from '../../../utility/deviceDimensions';
import { faceLogin } from '../../../utility/FacebookLogin';
import fonts from '../../../utility/fonts';
import images from '../../../utility/imagesPath';
import Loader from '../../../utility/Loader';
import MyView from '../../../utility/MyView';
import string from '../../../utility/string';
import StringConstants from '../../../utility/StringConstants';
import Utility from '../../../utility/Utility';
const emailvalid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

export default class Login extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: null,
            password: null,
            checked: false,
            visible: false,
            forgotEmail: null,
            accessToken: null,
            phoneView: true,
            forgotPasswordView: true,
            phoneNumber: null,
            phoneNumberToLogin: null,
            userId: null,
            loaderVisible: false,
            referCode: "",
        }
    }

    /**
     * submit your otp
     */
    submit() {
        if (this.state.phoneNumberToLogin == null || this.state.phoneNumberToLogin === "") {
            alert(string.enterPhoneNumber)
        } else if (this.state.phoneNumberToLogin.length < 6) {
            alert(string.phoneNumberLength)
        } else {
            this.setState({ loaderVisible: true });
            var phone = this.phoneeLogin.getValue().replace("+", "") + this.state.phoneNumberToLogin;
            ApiRequestManager.getInstance().requestServer("POST", 'phone=' + phone.toString() + "", APIURLS.LOGIN_URL, "").then((response) => {
                this.setState({ loaderVisible: false });
                var _this = this;
                setTimeout(function () {
                    _this.clearLoginFields();
                    _this.props.navigation.navigate("verification", { "userId": response.userId, "type": 'User' });
                    console.log("Result is ", JSON.stringify(response));
                }, 1000)
            }).catch((error) => {
                this.setState({ loaderVisible: false });
                setTimeout(function () {
                    alert(error);
                }, 1000)
            })
        }
    }

    /**
     * for remembering user login fields
     * @param {*} response 
     */
    saveTokenAndRememberFields(response) {
        if (this.state.checked) {
            let userDetails = {
                'EMAIL': this.state.email,
                'PASSWORD': this.state.password,
                'CHECKBOX_CHECKED': true,
            }
            AsyncStorage.setItem("DETAILS", JSON.stringify(userDetails));
            Utility.getInstance().setStoreData(StringConstants.key_user_token, response.authorization);
        } else {
            let userDetails = {
                'EMAIL': null,
                'PASSWORD': null,
                'CHECKBOX_CHECKED': false,
            }
            AsyncStorage.setItem("DETAILS", JSON.stringify(userDetails));
            Utility.getInstance().setStoreData(StringConstants.key_user_token, response.authorization);
        }
    }

    /**
     * login request manager
     */
    loginViaFacebook() {
        var loginRequestPromise = faceLogin();
        loginRequestPromise.then((result) => {
            this.facebookAPI(result);
        }).catch((error) => {
            console.log('Data  gotData' + JSON.stringify(error));
        })
    }

    /**
     * Api response for facebook login
     * @param {} token 
     */
    facebookAPI(token) {
        this.setState({ loaderVisible: true });
        ApiRequestManager.getInstance().requestServer("POST", "access_token=" + token + "", APIURLS.FACEBOOKLOGIN_URL, "").then((response) => {
            this.setState({ loaderVisible: false });
            var _this = this;
            setTimeout(function () {

                // show dialog and verify otp
                if (response.isFirstLogin) {
                    _this.setState({ userId: response.userId })
                    _this.setState({ visible: true, forgotPasswordView: true, phoneView: false });
                } else {
                    Utility.getInstance().setStoreData(StringConstants.key_user_token, response);
                    //if (response.data.isRegistered) {
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [
                            NavigationActions.navigate({ routeName: "navigationDrawer" })
                        ]
                    });
                    _this.props.navigation.dispatch(resetAction);
                    //}
                }
            }, 1000)
        }).catch((error) => {
            this.setState({ loaderVisible: false });
            setTimeout(function () {
                alert(error);
            }, 1000)
        })
    }

    /**
     * phone and other fields validations
     */
    updatePhone() {
        if (this.state.email === null || this.state.email.trim === "") {
            alert(string.enterEmail)
        } else if (emailvalid.test(this.state.email) === false) {
            alert(string.enterValidEmail)
        } else if (this.state.phoneNumber == null || this.state.phoneNumber.trim === "") {
            alert(string.enterPhoneNumber)
        } else if (this.state.phoneNumber.length < 6) {
            alert(string.phoneNumberLength)
        } else {
            var phone = this.phonee.getValue().replace("+", "") + this.state.phoneNumber;
            this.setState({ loaderVisible: true });
            ApiRequestManager.getInstance().requestServer("POST", 'phone=' + phone.toString() + "&userId=" + this.state.userId + "&email=" + this.state.email + "&referCode=" + this.state.referCode + "", APIURLS.UPDATEPHONE_URL, "").then((response) => {
                this.setState({ loaderVisible: false });
                var _this = this;
                setTimeout(function () {
                    if (response.type === "success") {
                        _this.clearLoginFields();
                        _this.setState({ visible: false, phoneNumber: "" });
                        _this.props.navigation.navigate("verification", { "userId": response.userId, "type": 'User' });
                    }
                }, 1000)
            }).catch((error) => {
                this.setState({ loaderVisible: false });
                setTimeout(function () {
                    alert(error);
                }, 1000)
            })
        }
    }

    /**
     * move to registeration page
     */
    moveToRegisterationPage() {
        // clear fields
        this.clearLoginFields();
        this.props.navigation.navigate("signUp");
    }

    /**
     * clear login fields
     */
    clearLoginFields() {
        this.setState({ email: "", password: "", phoneNumberToLogin: "" });
    }

    /**
     * Api response on forget password
     */
    forgotPassword() {
        if (this.state.forgotEmail == null || this.state.forgotEmail.replace(/^\s+|\s+$/g, "") === "") {
            alert("Please enter Email")
        } else if (emailvalid.test(this.state.forgotEmail.replace(/^\s+|\s+$/g, "")) === false) {
            alert("Invalid Email address")
        } else {
            this.setState({ loaderVisible: true });
            ApiRequestManager.getInstance().requestServer("POST", "email=" + this.state.forgotEmail + "&method=sms", APIURLS.FORGOTPASSWORD_URL, "").then((response) => {
                this.setState({ loaderVisible: false });
                var _this = this;
                setTimeout(function () {
                    _this.setState({ visible: false, forgotEmail: "" });
                    alert(response.message);
                    console.log("Forgot password response:", JSON.stringify(response));
                }, 1000)
            }).catch((error) => {
                this.setState({ loaderVisible: false });
                setTimeout(function () {
                    alert(error);
                }, 1000)
            })
        }
    }

    /**
     * UI part for login
     */
    render() {
        return (
            <SafeAreaView style={{ flex: 1, flexDirection: 'column' }}>
                <ImageBackground source={images.backGroundImage} style={{ flex: 1, flexDirection: 'column' }}>
                    <KeyboardAwareScrollView>
                        <Image source={images.logoImage} style={{ alignSelf: 'center', marginTop: 80 }} />

                        <View style={{ flex: 1, marginTop: 35, flexDirection: 'column', marginLeft: 40, marginRight: 40 }}>

                            <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                <View style={{ flex: 0.1, marginLeft: 18, marginRight: 6 }}>
                                    <Image style={{ height: 18, width: 18, }} source={images.telephone} color="#000" />
                                </View>
                                <PhoneInput style={{ margin: 2, flex: 0.1 }} ref={ref => {
                                    this.phoneeLogin = ref;
                                }} />
                                <TextInput keyboardType='numeric' maxLength={18} style={{ flex: 0.8, fontFamily: fonts.regularFont }} placeholder="Phone Number" placeHolderTextColor={colors.gray} onChangeText={phoneNumberToLogin => this.setState({ phoneNumberToLogin })} value={this.state.phoneNumberToLogin} style={{ flex: 0.9, height: 42 }} />
                            </View>
                            <Loader loading={this.state.loaderVisible} />

                            <TouchableOpacity onPress={this.submit.bind(this)} style={{ alignItems: "center", justifyContent: "center", height: 50, borderRadius: 25, backgroundColor: colors.buttonBorderColor, marginTop: 20, marginBottom: 40 }}>
                                <Text style={{ fontSize: 20, color: 'white', fontFamily: fonts.boldFont, color: 'white' }}>Login</Text>
                            </TouchableOpacity>

                            <Dialog visible={this.state.visible}
                                onTouchOutside={() => {
                                    this.setState({ visible: false });
                                }} backgroundColor='black'>
                                <DialogContent>
                                    <View style={{ flexDirection: 'column' }}>
                                        <MyView hide={this.state.forgotPasswordView}>
                                            <Text style={{ fontFamily: fonts.mediumFont, alignSelf: 'center', margin: 20, fontSize: 15 }}> Please enter your Email id</Text>
                                            <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                                <View style={{ flex: 0.1, marginLeft: 18, marginRight: 6 }}>
                                                    <Image style={{ height: 14, width: 18, }} source={images.email} color="#000" />
                                                </View>
                                                <TextInput keyboardType='email-address' style={{ fontFamily: fonts.regularFont }} placeholder="Email id" placeHolderTextColor={colors.gray} onChangeText={forgotEmail => this.setState({ forgotEmail })} value={this.state.forgotEmail} style={{ flex: 0.9, height: 42 }} />
                                            </View>

                                            <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                                <TouchableOpacity style={{ width: scaledWidthPercent(30), height: scaledHeight(35), padding: 5, borderRadius: 20, backgroundColor: colors.themeColor, justifyContent: 'center', alignItems: 'center', marginRight: 10, marginLeft: 10, marginTop: scaledHeight(15), marginBottom: 1 }}
                                                    onPress={() => {
                                                        this.setState({ visible: false, forgotEmail: "" });
                                                    }}>
                                                    <Text style={{ fontSize: 16, color: 'white', fontFamily: fonts.regularFont }}>Cancel</Text>
                                                </TouchableOpacity>

                                                <TouchableOpacity style={{ width: scaledWidthPercent(30), height: scaledHeight(35), padding: 5, borderRadius: 20, backgroundColor: colors.themeColor, justifyContent: 'center', alignItems: 'center', marginLeft: 10, marginRight: 10, marginTop: scaledHeight(15), marginBottom: 1 }} onPress={this.forgotPassword.bind(this)}>
                                                    <Text style={{ fontSize: 16, color: 'white', fontFamily: fonts.regularFont }}>Ok</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </MyView>

                                        <MyView hide={this.state.phoneView}>
                                            <View style={{ margin: 20 }}>
                                                <Text style={{ marginBottom: 10, fontFamily: fonts.mediumFont, alignSelf: 'center', fontSize: 15 }}>Please enter following details</Text>
                                                <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                                    <View style={{ flex: 0.1, marginLeft: 18, marginRight: 6 }}>
                                                        <Image style={{ height: 15, width: 20, }} source={images.email} color="#000" />
                                                    </View>
                                                    <TextInput style={{ flex: 0.9, fontFamily: fonts.regularFont }} placeholder="Email" placeHolderTextColor={colors.gray} onChangeText={email => this.setState({ email })} value={this.state.email} style={{ flex: 0.9, height: 42 }} />
                                                </View>

                                                <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                                    <View style={{ flex: 0.1, marginLeft: 18, marginRight: 6 }}>
                                                        <Image style={{ height: 18, width: 18, }} source={images.telephone} color="#000" />
                                                    </View>
                                                    <PhoneInput style={{ margin: 2, flex: 0.1 }} ref={ref => {
                                                        this.phonee = ref;
                                                    }} />
                                                    <TextInput keyboardType='numeric' maxLength={10} style={{ flex: 0.8, fontFamily: fonts.regularFont }} placeholder="Phone number" placeHolderTextColor={colors.gray} onChangeText={phoneNumber => this.setState({ phoneNumber })} value={this.state.phoneNumber} style={{ flex: 0.9, height: 42 }} />
                                                </View>

                                                <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                                    <View style={{ flex: 0.1, marginLeft: 18, marginRight: 6 }}>
                                                        <Image style={{ height: 15, width: 20, }} source={images.email} color="#000" />
                                                    </View>
                                                    <TextInput style={{ flex: 0.9, fontFamily: fonts.regularFont }} placeholder="Promo Code" placeHolderTextColor={colors.gray} onChangeText={referCode => this.setState({ referCode })} value={this.state.referCode} style={{ flex: 0.9, height: 42 }} />
                                                </View>

                                                <View style={{ flexDirection: 'row', marginTop: 20 }}>
                                                    <TouchableOpacity style={{ width: scaledWidthPercent(30), height: scaledHeight(35), padding: 5, borderRadius: 20, backgroundColor: colors.themeColor, justifyContent: 'center', alignItems: 'center', marginRight: 10, marginLeft: 10, marginTop: scaledHeight(15), marginBottom: 1 }} onPress={() => { this.setState({ visible: false, phoneNumber: "" }); }}>
                                                        <Text style={{ fontSize: 16, color: 'white', fontFamily: fonts.regularFont }}>Cancel</Text>
                                                    </TouchableOpacity>

                                                    <TouchableOpacity style={{ width: scaledWidthPercent(30), height: scaledHeight(35), padding: 5, borderRadius: 20, backgroundColor: colors.themeColor, justifyContent: 'center', alignItems: 'center', marginLeft: 10, marginRight: 10, marginTop: scaledHeight(15), marginBottom: 1 }} onPress={this.updatePhone.bind(this)}>
                                                        <Text style={{ fontSize: 16, color: 'white', fontFamily: fonts.regularFont }}>Ok</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </MyView>
                                    </View>
                                </DialogContent>
                            </Dialog>

                            <TouchableOpacity onPress={this.loginViaFacebook.bind(this)} style={{ alignItems: "center", justifyContent: "center", height: 50, borderRadius: 25, backgroundColor: colors.facebookColor, }}>
                                <View style={{ flexDirection: "row", alignItems: 'center' }}>
                                    <Image source={images.fbIcon} style={{ marginRight: 10 }} />
                                    <Text style={{ fontSize: 20, color: 'white', fontFamily: fonts.boldFont, color: 'white' }}>Facebook</Text>
                                </View>
                            </TouchableOpacity>

                            <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center', marginTop: 60 }}>
                                <Text style={{ color: 'black', fontSize: 12, fontFamily: fonts.regularFont }}>New Member? </Text>
                                <TouchableOpacity onPress={this.moveToRegisterationPage.bind(this)}>
                                    <Text style={{ color: 'black', fontSize: 12, textDecorationLine: 'underline', fontFamily: fonts.boldFont }}>Signup here</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </ImageBackground>
            </SafeAreaView>
        );
    }
}
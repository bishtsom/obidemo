import React, { Component } from 'react';
import { Image, ImageBackground, PermissionsAndroid, Platform, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Permissions from 'react-native-permissions';
import { NavigationActions, SafeAreaView, StackActions } from 'react-navigation';
import backGroundImage from '../../../images/bg.png';
import logoImage from '../../../images/logo.png';
import { scaledHeight } from '../../../utility/deviceDimensions';
import fonts from '../../../utility/fonts';
import StringConstants from '../../../utility/StringConstants';
import Utility from '../../../utility/Utility';
import colors from '../../../utility/colors';

export default class SignUpLogin extends Component {
    constructor(props) {
        super(props)
        this.state = {
            accessToken: null,
        }
    }

    /**
     * For fetching access token saved in device preferences
     */
    componentWillMount() {
        Utility.getInstance()
            .getStoreData(StringConstants.key_user_token)
            .then(data => {
                console.log('token is' + data);
                if (data != undefined) {
                    console.log("data", data);
                    if (JSON.parse(data).data.type == "driver") {

                        console.log('redirecting to');
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: "driverNavigation" })
                            ]
                        });
                        this.props.navigation.dispatch(resetAction);
                    } else {
                        console.log('redirecting to');
                        const resetAction = StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: "navigationDrawer" })
                            ]
                        });
                        this.props.navigation.dispatch(resetAction);
                    }

                } else {
                    console.log('not redirecting to');
                }
            })
            .catch(error => {
                console.log("error is " + error);
            });
    }

    /**
     * Get permissions update for locations
     */
    componentDidMount() {
        if (Platform.OS == 'android') {
            Permissions.check('photo').then(response => {
                // Response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
                if (response != 'authorized') {
                    PermissionsAndroid.request(
                        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                        {
                            'title': 'Obi need Location Permission',
                            'message': 'Obi need to access your current Location' +
                                'to be more accurate'
                        }
                    ).then((granted) => {
                        if (granted === PermissionsAndroid.RESULTS.GRANTED) {

                        } else {
                            console.log("Camera permission denied")
                        }
                    }).
                        catch((err) => {
                            console.warn(err)
                        })
                    PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION, {
                        'title': 'Obi need Location Permission',
                        'message': 'Obi need to access your current Location' +
                            'to be more accurate'
                    }
                    ).then((granted) => { }).
                        catch((err) => {
                            console.warn(err)
                        })
                }
            })
        }

        Utility.getInstance().getOneTimeCurrentLocation().then((value) => {
        }).catch((error) => {
        })
    }

    /**
     * Signup if you are a new rider
     */
    signUp() {
        this.props.navigation.navigate("signUp");
    }

    /**
     * login to the app as a rider
     */
    login() {
        this.props.navigation.navigate("login");
    }

    /**
     * login as a driver 
     */
    loginDriver() {
        this.props.navigation.navigate("loginDriver");
    }

    /**
     * Sign up page UI part
     */
    render() {
        return (
            <SafeAreaView style={{ flex: 1, flexDirection: 'column' }}>
                <ImageBackground source={backGroundImage} style={{ flex: 1, flexDirection: 'column' }}>

                    <Image source={logoImage} style={{ alignSelf: 'center', marginTop: scaledHeight(90) }} />

                    <View style={{ marginTop: scaledHeight(120), justifyContent: 'center', flexDirection: 'column', alignItems: "center" }}>
                        <TouchableOpacity onPress={this.signUp.bind(this)} style={{ alignItems: "center", justifyContent: "center", height: 50, width: 250, borderRadius: 25, backgroundColor: colors.themeColor }}>
                            <Text style={{ fontSize: 20, color: 'white', fontStyle: "normal" }}>Sign up</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.login.bind(this)} style={{ alignItems: "center", justifyContent: "center", height: 50, width: 250, borderRadius: 25, color: "white", marginTop: 15, borderColor: colors.themeColor, borderWidth: 2 }}>
                            <Text style={{ fontSize: 20, color: colors.themeColor, fontStyle: 'normal', fontFamily: fonts.boldFont }}>Login</Text>
                        </TouchableOpacity>

                        <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center', marginTop: 40 }}>
                            <Text style={{ color: 'black', fontSize: 12, fontFamily: fonts.regularFont }}>If you are driver? </Text>
                            <TouchableOpacity onPress={this.loginDriver.bind(this)}>
                                <Text style={{ color: 'black', fontSize: 12, textDecorationLine: 'underline', fontFamily: fonts.boldFont }}>Login here</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ImageBackground>
            </SafeAreaView>
        );
    }
}
import moment from "moment";
import React, { Component } from "react";
import { Image, Text, TextInput, View } from "react-native";
import BraintreeDropIn from "react-native-braintree-dropin-ui";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import ImagePicker from "react-native-image-picker";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DateTimePicker from "react-native-modal-datetime-picker";
import { NavigationActions, SafeAreaView, StackActions } from "react-navigation";
import ApiRequestManager from "../../../requestManager/ApiRequestManager";
import { APIURLS } from "../../../requestManager/UrlManager";
import colors from "../../../utility/colors";
import CustomHeader from "../../../utility/CustomHeader";
import fonts from "../../../utility/fonts";
import imagesPath from "../../../utility/imagesPath";
import Loader from "../../../utility/Loader";
import string from "../../../utility/string";
import StringConstants from "../../../utility/StringConstants";
import Utility from "../../../utility/Utility";

emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

export default class EditProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: null,
      lastName: null,
      email: null,
      phoneNumber: null,
      birthday: "Birthday",
      address: "Address",
      imageUri: "",
      borderWidthValue: 0,
      borderValue: 0,
      textColor: colors.gray,
      isDateTimePickerVisible: false,
      accessToken: null,
      loaderVisible: false,
      phoneNumberColor: colors.gray,
      clientToken: null,
      nonce: null,
      specialChars: "<>@!#$%^&*()_+[]{}?:;|'\"\\,./~`-=",
      homeLocation: {
        lat: "",
        lng: "",
      }
    };
  }

  /**
   * get the user access token
   */
  componentDidMount() {
    Utility.getInstance()
      .getStoreData(StringConstants.key_user_token)
      .then(data => {
        try {
          let user_data = JSON.parse(data);
          this.setState({ accessToken: user_data.authorization });
          setTimeout(
            () =>
              ApiRequestManager.getInstance()
                .requestServer("GET", "", APIURLS.GETPROFILE_URL, "Bearer " + user_data.authorization)
                .then(response => {
                  console.log("profile response", JSON.stringify(response));
                  if ('homeLocation' in response.data) {
                    this.setState({ homeLocation: response.data.homeLocation });
                  }
                  if (response.data.firstName != null || response.data.firstName != "") {
                    this.setState({ firstName: response.data.firstName });
                  }
                  if (response.data.lastName != null || response.data.lastName != "") {
                    this.setState({ lastName: response.data.lastName });
                  }
                  if (response.data.email != null || response.data.email != "") {
                    this.setState({ email: response.data.email });
                  }
                  if ('birthday' in response.data) {
                    if (response.data.birthday != null || response.data.birthday != "") {
                      this.setState({ birthday: response.data.birthday, textColor: "black" });
                    }
                  }
                  if (response.data.phone != null || response.data.phone != "") {
                    var pNumber = response.data.phone.replace(/\s+/g, "");
                    this.setState({ phoneNumber: pNumber, phoneNumberColor: "black" });
                  }
                  if ('address' in response.data) {
                    if (response.data.address != null || response.data.address != "") {
                      this.setState({ address: response.data.address });
                    }
                  }
                  if (response.data.photo != undefined) {
                    this.setState({ imageUri: response.data.photo });
                  }
                })
                .catch(error => {
                  alert(error);
                }),
            1000
          );
        } catch (exp) {
          console.log("exception" + exp);
        }
      })
      .catch(err => {
        console.log("error is" + err);
      });
  }

  _showDateTimePicker = () => this.setState({ isDateTimePickerVisible: true });

  _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });

  _handleDatePicked = date => {
    var newDate = moment(date).format("MM-DD-YYYY");
    this.setState({ birthday: newDate, textColor: "black" });
    this._hideDateTimePicker();
  };

  /**
   * Sign up page validations
   */
  signUpPageEntriesValidation() {
    if (this.state.firstName == null || this.state.firstName === "") {
      alert("Please enter your first name");
    } else if (this.checkForSpecialChar(this.state.firstName)) {
      alert('Enter valid first name')
    } else if (this.state.lastName == null || this.state.lastName === "") {
      alert("Please enter your last name");
    } else if (this.checkForSpecialChar(this.state.lastName)) {
      alert('Enter valid last name')
    } else if (this.state.phoneNumber == null || this.state.phoneNumber === "") {
      alert(string.enterPhoneNumber);
    } else if (!this.state.phoneNumber.length === 13) {
      alert(string.phoneNumberLength);
    } else if (this.state.birthday == null || this.state.birthday === "Birthday") {
      alert("please select your birth date");
    } else if (this.state.address == null || this.state.address === "Address" || this.state.address == "") {
      alert("Please enter your address");
    } else {
      this.saveProfile();
    }
  }

  /**
   * check special character
   * @param {*} name 
   */
  checkForSpecialChar(name) {
    for (i = 0; i < this.state.specialChars.length; i++) {
      if (name.indexOf(this.state.specialChars[i]) > -1) {
        return true
      }
    }
    return false;
  }

  // application terms and condition page
  termsAndCondition() { }

  imageLoadingStart = () => this.setState({ loaderVisible: true });

  imageLoadingEnd = () => this.setState({ loaderVisible: false });

  /**
   * Get the uri response
   */
  selectImageFunctioning() {
    ImagePicker.showImagePicker(null, response => {
      console.log("Response = ", response);
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        const source = { uri: response.uri };
        setTimeout(() => {
          this.setState({ imageUri: response.uri });
        }, 200);
      }
    });
  }

  /**
   * signup to the app with user entries
   */
  saveProfile() {
    this.setState({ loaderVisible: true });

    const requestBody = new FormData();
    requestBody.append("firstName", this.state.firstName);
    requestBody.append("lastName", this.state.lastName);
    requestBody.append("email", this.state.email);
    requestBody.append("birthday", this.state.birthday);
    requestBody.append("address", this.state.address);
    requestBody.append('latitude', this.state.homeLocation.lat)
    requestBody.append('longitude', this.state.homeLocation.lng)
    if (!(this.state.imageUri.toString().includes('http://'))) {
      console.log("contains");
    } else {
      console.log("Not contains");
    }
    console.log("image uri", this.state.imageUri);
    if (this.state.imageUri != "") {
      if (!(this.state.imageUri.toString().includes('http://'))) {
        requestBody.append("image", {
          uri: this.state.imageUri,
          type: "multipart/form-data",
          name: "profileImage.jpg"
        });
      }
    }

    /**
     * Update profile to server
     */
    ApiRequestManager.getInstance()
      .requestServerProfile("POST", APIURLS.PROFILEUPDATE_URL, "Bearer " + this.state.accessToken, requestBody)
      .then(response => {
        this.setState({ loaderVisible: false });
        var _this = this;
        setTimeout(function () {
          Utility.getInstance().setStoreData(StringConstants.key_home_region, _this.state.homeLocation);
          alert(response.message);
          if (_this.props.navigation.state.params.route == "NO") {
            _this.props.navigation.navigate("navigationDrawer");
          } else {
            const resetAction = StackActions.reset({
              index: 0,
              actions: [
                NavigationActions.navigate({ routeName: "navigationDrawer" })
              ]
            });
            _this.props.navigation.dispatch(resetAction);
          }
        }, 1000);
      })
      .catch(error => {
        this.setState({ loaderVisible: false });
        setTimeout(function () {
          alert(error.request);
        }, 1000);
      });
  }

  /**
   * Brain tree implementation for payment
   */
  brainTree() {
    BraintreeDropIn.show({
      clientToken: this.state.clientToken,
      merchantIdentifier: "applePayMerchantIdentifier",
      countryCode: "US", //apple pay setting
      currencyCode: "USD", //apple pay setting
      merchantName: "Your Merchant Name for Apple Pay",
      orderTotal: "Total Price",
      googlePay: true,
      applePay: true
    })
      .then(result => {
        console.log("nonceis", result);
        this.setState({ nonce: result.nonce });
        this.addCard();
      })
      .catch(error => {
        if (error.code === "USER_CANCELLATION") {
          // update your UI to handle cancellation
        } else {
          // update your UI to handle other errors
        }
      });
  }

  addCard() {
    ApiRequestManager.getInstance()
      .requestServer("POST", "nonce=" + this.state.nonce, APIURLS.ADDCARD_URL, "Bearer " + this.state.accessToken)
      .then(response => {
        console.log(response);
      })
      .catch(error => {
        alert(error);
      });
  }

  /**
   * Add card details
   */
  addCardMethod() {
    ApiRequestManager.getInstance()
      .requestServer("GET", "", APIURLS.GENERATEBRAINTREETOKEN_URL, "Bearer " + this.state.accessToken)
      .then(response => {
        this.setState({ clientToken: response.data });
        this.brainTree();
      })
      .catch(error => {
        alert(error);
      });
  }

  /**
   * moving back to previous screen
   */
  back() {
    if (this.props.navigation.state.params.route == "NO") {
      this.props.navigation.navigate("navigationDrawer");
    } else {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: "navigationDrawer" })]
      });
      this.props.navigation.dispatch(resetAction);
    }
  }

  updateData = data => {
    var NY = {
      lat: data.latitude,
      lng: data.longitude
    };
    this.setState({ homeLocation: NY });
    Utility.getInstance().getDetailedAddress(NY).then(value => {
      this.setState({ address: value.formattedAddress });
    })
  };

  /**
   * Updating home address
   */
  selectYourHomeAddress() {
    if (this.state.address == 'Address' || this.state.address == null) {
      this.props.navigation.navigate("setLocationOnMap", { updateData: this.updateData, currentLatLng: null });
    } else {
      Utility.getInstance().getLatLongViaAddress(this.state.address).then(data => {
        let region = {
          latitude: data.lat,
          longitude: data.lng,
          latitudeDelta: StringConstants.LATITUDE_DELTA,
          longitudeDelta: StringConstants.LONGITUDE_DELTA,
        }
        this.props.navigation.navigate("setLocationOnMap", { updateData: this.updateData, currentLatLng: region });
      }).catch((error) => {
        console.log(error);
      })
    }
  }

  /**
   * uploading profile image
   */
  imgLoad() {
    if (this.state.imageUri == "" || this.state.imageUri == "https://image.flaticon.com/icons/svg/149/149071.svg") {
      console.log("Haaaa")
    } else {
      console.log("Naa");
      this.setState({ borderValue: (120 / 2), borderWidthValue: 3 })
    }
  }

  render() {
    return (
      <SafeAreaView style={{ flex: 1, flexDirection: "column" }}>
        <CustomHeader LeftHeading={"Profile Creation"} RightHeading={"Add Card"} LeftImage={imagesPath.backWhite} rightClick={this.addCardMethod.bind(this)} leftClick={this.back.bind(this)} />
        <KeyboardAwareScrollView>
          <View style={{ flex: 1, flexDirection: "column" }}>
            <Text style={{ alignSelf: "center", marginTop: 30, fontFamily: fonts.regularFont }}>Profile Pic</Text>
            <View style={{ alignSelf: "center", marginTop: 10 }}>
              <TouchableOpacity onPress={this.selectImageFunctioning.bind(this)}              >
                <Image
                  onLoadEnd={this.imgLoad.bind(this)}
                  source={
                    this.state.imageUri == "" ||
                      this.state.imageUri ==
                      "https://image.flaticon.com/icons/svg/149/149071.svg"
                      ? imagesPath.profileImage
                      : { uri: this.state.imageUri }
                  }
                  style={{
                    width: 120, height: 120, borderRadius: this.state.borderValue,
                    borderWidth: this.state.borderWidthValue,
                    borderColor:
                      this.state.imageUri == "" ||
                        this.state.imageUri ==
                        "https://image.flaticon.com/icons/svg/149/149071.svg"
                        ? ""
                        : colors.themeColor
                  }}>
                </Image>
              </TouchableOpacity>
            </View>
            <ScrollView>
              <View style={{ flex: 1, justifyContent: "center", flexDirection: "column", alignItems: "center", alignSelf: "center", alignContent: "center" }}>

                <View style={{ flexDirection: "row", alignItems: "center", height: 40, width: 290, borderRadius: 25, color: "white", marginTop: 30, borderColor: "#32A582", borderWidth: 1 }}>
                  <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                    <Image style={{ height: 18, width: 12 }} source={imagesPath.userName} color="#000" />
                  </View>
                  <TextInput maxLength={15} style={{ flex: 0.9, height: 42, fontFamily: fonts.regularFont }}
                    placeholder="First Name" placeHolderTextColor={colors.gray}
                    onChangeText={firstName => this.setState({ firstName })} value={this.state.firstName} />
                </View>

                <View style={{ flexDirection: "row", alignItems: "center", height: 40, width: 290, borderRadius: 25, color: "white", marginTop: 18, borderColor: "#32A582", borderWidth: 1 }}>
                  <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                    <Image style={{ height: 18, width: 12 }} source={imagesPath.userName} color="#000" />
                  </View>
                  <TextInput maxLength={15} style={{ flex: 0.9, height: 42, fontFamily: fonts.regularFont }}
                    placeholder="Last Name"
                    placeHolderTextColor={colors.gray}
                    onChangeText={lastName => this.setState({ lastName })}
                    value={this.state.lastName}
                  />
                </View>

                <View style={{ flexDirection: "row", alignItems: "center", height: 40, width: 290, borderRadius: 25, color: "white", marginTop: 18, borderColor: "#32A582", borderWidth: 1 }}                >
                  <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                    <Image style={{ height: 16, width: 20 }} source={imagesPath.email} color="#000" />
                  </View>
                  <TextInput editable={false} style={{ flex: 0.9, height: 42, fontFamily: fonts.regularFont }}
                    placeholder="Email"
                    placeHolderTextColor={colors.gray}
                    onChangeText={email => this.setState({ email })}
                    value={this.state.email}
                  />
                </View>

                <View style={{ flexDirection: "row", alignItems: "center", height: 40, width: 290, borderRadius: 25, color: "white", marginTop: 18, borderColor: "#32A582", borderWidth: 1 }}>
                  <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                    <Image style={{ height: 16, width: 16 }} source={imagesPath.telephone} color="#000" />
                  </View>
                  <TextInput
                    editable={false}
                    style={{ color: this.state.phoneNumberColor, flex: 0.9, height: 42, fontFamily: fonts.regularFont }}
                    placeholder="Phone No."
                    placeHolderTextColor={colors.gray}
                    onChangeText={phoneNumber => this.setState({ phoneNumber })}
                    value={this.state.phoneNumber}
                    keyboardType="numeric"
                  />
                </View>

                <TouchableOpacity
                  onPress={this._showDateTimePicker.bind(this)}
                  style={{ justifyContent: "center", flexDirection: "row", alignItems: "center", height: 40, width: 290, borderRadius: 25, color: "white", marginTop: 18, borderColor: "#32A582", borderWidth: 1 }}>
                  <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                    <Image style={{ height: 16, width: 16 }} source={imagesPath.birthday} color="#000" />
                  </View>
                  <Text placeholder="Birthday" style={{ color: this.state.textColor, flex: 0.9, fontFamily: fonts.regularFont }}>
                    {this.state.birthday}
                  </Text>
                  <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                    maximumDate={new Date()}
                  />
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={this.selectYourHomeAddress.bind(this)}
                  style={{ flexDirection: "row", alignItems: "center", justifyContent: 'center', height: 40, width: 290, borderRadius: 25, color: "white", marginTop: 18, borderColor: "#32A582", borderWidth: 1 }}>
                  <View style={{ flex: 0.1, marginLeft: 18, marginRight: 4 }}>
                    <Image style={{ height: 16, width: 16 }} source={imagesPath.address} color="#000" />
                  </View>
                  <Text style={{ flex: 0.9, fontFamily: fonts.regularFont }} numberOfLines={1}>{this.state.address}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                  onPress={this.signUpPageEntriesValidation.bind(this)}
                  style={{ alignItems: "center", justifyContent: "center", height: 50, marginTop: 22, marginBottom: 15, width: 290, borderRadius: 25, backgroundColor: "#32A582" }}>
                  <Text style={{ fontSize: 20, color: "white", fontFamily: fonts.boldFont, color: "white" }}>Save</Text>
                </TouchableOpacity>

                <Loader loading={this.state.loaderVisible} />
              </View>
            </ScrollView>
          </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

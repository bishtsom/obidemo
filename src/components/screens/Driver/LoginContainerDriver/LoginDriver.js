import React, { Component } from 'react';
import { ImageBackground, Image, View, Text, TextInput, AsyncStorage } from 'react-native';
import colors from '../../../../utility/colors';
import PhoneInput from 'react-native-phone-input'
import images from '../../../../utility/imagesPath';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-navigation';
import fonts from '../../../../utility/fonts';
import { APIURLS } from '../../../../requestManager/UrlManager';
import { faceLogin } from '../../../../utility/FacebookLogin';
import AppStorage from '../../../../utility/AppStorage';
import string from '../../../../utility/string';
import Loader from '../../../../utility/Loader';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ApiRequestManager from '../../../../requestManager/ApiRequestManager';
const FBSDK = require('react-native-fbsdk');

const emailvalid = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

export default class LoginDriver extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: null,
            password: null,
            checked: false,
            visible: false,
            forgotEmail: null,
            accessToken: null,
            phoneView: true,
            forgotPasswordView: true,
            phoneNumberToLogin: '',
            phoneNumber: null,
            userId: null,
            loaderVisible: false,
        }
    }

    componentDidMount() {
        AsyncStorage.getItem("DETAILS").then((value) => {
            if (JSON.parse(value).CHECKBOX_CHECKED) {
                // show email and password
                this.setState({ email: JSON.parse(value).EMAIL, password: JSON.parse(value).PASSWORD });
            }
        })
    }
    // submit your otp
    submit() {
        if (this.state.phoneNumberToLogin == null || this.state.phoneNumberToLogin === "") {
            alert(string.enterPhoneNumber)
        } else if (this.state.phoneNumberToLogin.length < 6) {
            alert(string.phoneNumberLength)
        } else {
            this.setState({ loaderVisible: true });
            var phone = this.phoneeLogin.getValue().replace("+", "") + this.state.phoneNumberToLogin;
            ApiRequestManager.getInstance().requestServer("POST", 'phone=' + phone.toString() + "", APIURLS.DRIVER_LOGIN_URL, "").then((response) => {
                this.setState({ loaderVisible: false });
                var _this = this;
                setTimeout(function () {
                    _this.clearLoginFields();
                    _this.props.navigation.navigate("verification", { "userId": response.userId, "type": 'Driver' });
                    console.log("Result is ", JSON.stringify(response));
                }, 1000)
            }).catch((error) => {
                this.setState({ loaderVisible: false });
                setTimeout(function () {
                    alert(error);
                }, 1000)
            })
        }
    }

    saveTokenAndRememberFields(response) {
        if (this.state.checked) {
            let userDetails = {
                'EMAIL': this.state.email,
                'PASSWORD': this.state.password,
                'CHECKBOX_CHECKED': true,
            }
            AsyncStorage.setItem("DETAILS", JSON.stringify(userDetails));
            AppStorage.getInstance().setStoreData("ACCESS_TOKEN", response.authorization);
        } else {
            let userDetails = {
                'EMAIL': null,
                'PASSWORD': null,
                'CHECKBOX_CHECKED': false,
            }
            AsyncStorage.setItem("DETAILS", JSON.stringify(userDetails));
            AppStorage.getInstance().setStoreData("ACCESS_TOKEN", response.authorization);
        }
    }

    // login request manager
    loginViaFacebook() {
        var loginRequestPromise = faceLogin();
        loginRequestPromise.then((result) => {
            this.facebookAPI(result);
        }).catch((error) => {
            console.log('Data  gotData' + JSON.stringify(error));
        })
    }

    facebookAPI(token) {
        this.setState({ loaderVisible: true });
        ApiRequestManager.getInstance().requestServer("POST", "access_token=" + token + "", APIURLS.FACEBOOKLOGIN_URL, "").then((response) => {
            this.setState({ loaderVisible: false });
            var _this = this;
            setTimeout(function () {
                if (response.type === "success") {
                    if (response.isPhoneVerified === true) {
                        if (response.authorization != null || response.authorization != "") {
                            // existing user
                            _this.clearLoginFields();
                            _this.props.navigation.navigate("navigationDrawer");
                        }
                    } else {
                        // show dialog and verify otp
                        _this.setState({ userId: response.userId })
                        _this.setState({ visible: true, forgotPasswordView: true, phoneView: false });
                    }
                }
            }, 1000)
        }).catch((error) => {
            this.setState({ loaderVisible: false });
            setTimeout(function () {
                alert(error);
            }, 1000)
        })
    }

    updatePhone() {
        if (this.state.phoneNumber == null || this.state.phoneNumber.trim === "") {
            alert(string.enterPhoneNumber)
        }
        else if (this.state.phoneNumber.length < 6) {
            alert(string.phoneNumberLength)
        } else {
            var phone = this.state.phoneNumber;
            this.setState({ loaderVisible: true });
            ApiRequestManager.getInstance().requestServer("POST", 'phone=' + phone.toString() + "&userId=" + this.state.userId + "", APIURLS.UPDATEPHONE_URL, "").then((response) => {
                this.setState({ loaderVisible: false });
                var _this = this;
                setTimeout(function () {
                    if (response.type === "success") {
                        _this.clearLoginFields();
                        _this.setState({ visible: false, phoneNumber: "" });
                        _this.props.navigation.navigate("verification", { "userId": response.userId });
                    }
                }, 1000)
            }).catch((error) => {
                this.setState({ loaderVisible: false });
                setTimeout(function () {
                    alert(error);
                }, 1000)
            })
        }
    }

    moveToRiderLoginPage() {
        // clear fields
        this.clearLoginFields();
        this.props.navigation.navigate("login");
    }

    // clear login fields
    clearLoginFields() {
        this.setState({ email: "", password: "" });
    }

    forgotPassword() {
        if (this.state.forgotEmail == null || this.state.forgotEmail.replace(/^\s+|\s+$/g, "") === "") {
            alert("Please enter Email")
        } else if (emailvalid.test(this.state.forgotEmail.replace(/^\s+|\s+$/g, "")) === false) {
            alert("Invalid Email address")
        } else {
            this.setState({ loaderVisible: true });
            ApiRequestManager.getInstance().requestServer("POST", "email=" + this.state.forgotEmail + "&method=sms", APIURLS.FORGOTPASSWORD_URL, "").then((response) => {
                this.setState({ loaderVisible: false });
                var _this = this;
                setTimeout(function () {
                    _this.setState({ visible: false, forgotEmail: "" });
                    alert(response.message);
                    console.log("Forgot password response:", JSON.stringify(response));
                }, 1000)
            }).catch((error) => {
                this.setState({ loaderVisible: false });
                setTimeout(function () {
                    alert(error);
                }, 1000)
            })
        }
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, flexDirection: 'column' }}>
                <ImageBackground source={images.backGroundImage} style={{ flex: 1, flexDirection: 'column' }}>
                    <KeyboardAwareScrollView>
                        <Image source={images.logoImage} style={{ alignSelf: 'center', marginTop: 60 }} />
                        <Text style={{ marginTop: 10, alignSelf: "center", fontFamily: fonts.regularFont, fontSize: 10 }}>DRIVER</Text>
                        <View style={{ flex: 1, marginTop: 35, flexDirection: 'column', marginLeft: 40, marginRight: 40 }}>
                            <View style={{ flexDirection: 'row', alignItems: "center", height: 45, borderRadius: 25, color: "white", marginTop: 12, borderColor: '#32A582', borderWidth: 1 }}>
                                <View style={{ flex: 0.1, marginLeft: 18, marginRight: 6 }}>
                                    <Image style={{ height: 18, width: 18, }} source={images.telephone} color="#000" />
                                </View>
                                <PhoneInput style={{ margin: 2, flex: 0.1 }} ref={ref => {
                                    this.phoneeLogin = ref;
                                }} />
                                <TextInput keyboardType='numeric' maxLength={18} style={{ flex: 0.8, fontFamily: fonts.regularFont }} placeholder="Phone Number" placeHolderTextColor={colors.gray} onChangeText={phoneNumberToLogin => this.setState({ phoneNumberToLogin })} value={this.state.phoneNumberToLogin} style={{ flex: 0.9, height: 42 }} />
                            </View>
                            <Loader loading={this.state.loaderVisible} />
                            <TouchableOpacity onPress={this.submit.bind(this)} style={{ alignItems: "center", justifyContent: "center", height: 50, borderRadius: 25, backgroundColor: colors.buttonBorderColor, marginTop: 20 }}>
                                <Text style={{ fontSize: 20, color: 'white', fontFamily: fonts.boldFont, color: 'white' }}>Login</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAwareScrollView>

                    <View style={{ flexDirection: 'row', position: 'absolute', bottom: 40, alignSelf: 'center' }}>
                        <Text style={{ color: 'black', fontSize: 12, fontFamily: fonts.regularFont }}>If you are Rider? </Text>
                        <TouchableOpacity onPress={this.moveToRiderLoginPage.bind(this)}>
                            <Text style={{ color: 'black', fontSize: 12, textDecorationLine: 'underline', fontFamily: fonts.regularFont }}>Login here</Text>
                        </TouchableOpacity>
                    </View>
                </ImageBackground>
            </SafeAreaView>
        );
    }
}